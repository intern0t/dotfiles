:set tabstop=4
:set shiftwidth=4
:set expandtab

" Powerline
set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/

" Always show statusline
set laststatus=2

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

" Setting Powerline font
let g:airline_powerline_fonts = 1


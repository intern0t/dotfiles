#!/usr/bin/env bash
# Capture screenshot of active workspace.
screenshot="/tmp/i3lockscreenshot.png"
lockicon="/home/scarecr0w/.config/i3/avatar.png"

(( $# )) && { icon=$1; }

scrot "$screenshot"
convert "$screenshot" -scale 10% -scale 1000% "$screenshot"
convert "$screenshot" "$lockicon" -gravity West -composite -matte "$screenshot"
i3lock -u -i "$screenshot"
rm "$screenshot"
